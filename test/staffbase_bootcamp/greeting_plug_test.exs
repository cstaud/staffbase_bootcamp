defmodule StaffbaseBootcamp.GreetingPlugTest do
  use ExUnit.Case
  use Plug.Test

  describe "unit tests" do
    test "returns Invalid Argument Error when hour < 0" do
      greeting = StaffbaseBootcamp.GreetingPlug.get_greeting(-1)
      assert greeting == "Invalid Argument Error"
    end

    test "returns Invalid Argument Error when hour > 23" do
      greeting = StaffbaseBootcamp.GreetingPlug.get_greeting(24)
      assert greeting == "Invalid Argument Error"
    end

    test "returns Good Morning when hour >= 0 and hour < 12" do
      greeting = StaffbaseBootcamp.GreetingPlug.get_greeting(11)
      assert greeting == "Good Morning \x{1f602}"
    end

    test "returns Good Afternoon when hour >= 12 and hour < 17" do
      greeting = StaffbaseBootcamp.GreetingPlug.get_greeting(15)
      assert greeting == "Good Afternoon \x{1f603}"
    end

    test "returns Good Afternoon when hour >= 17 and hour < 24" do
      greeting = StaffbaseBootcamp.GreetingPlug.get_greeting(19)
      assert greeting == "Good Evening \x{1f604}"
    end
  end

  test "plug test" do
    conn =
      :get
      |> conn("/", "")
      |> StaffbaseBootcamp.GreetingPlug.call(StaffbaseBootcamp.GreetingPlug.init([]))

    assert conn.state == :sent
    assert conn.status == 200
    assert String.contains?(conn.resp_body, "Good")
  end
end
