defmodule StaffbaseBootcamp.GreetingPlug do
  def init(options), do: options

  def call(conn, _options) do
    conn
    |> Plug.Conn.put_resp_content_type("text/plain")
    |> Plug.Conn.send_resp(200, get_greeting())
  end

  def get_greeting do
    hour = Time.utc_now().hour
    get_greeting(hour)
  end

  def get_greeting(hour) do
    cond do
      hour < 0 or hour > 23 ->
        "Invalid Argument Error"

      hour >= 0 and hour < 12 ->
        "Good Morning \x{1f602}"

      hour >= 12 and hour < 17 ->
        "Good Afternoon \x{1f603}"

      hour >= 17 and hour < 24 ->
        "Good Evening \x{1f604}"
    end
  end
end
