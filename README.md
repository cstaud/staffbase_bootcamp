# Staffbase Boocamp

## App
An app greeting the user with "Good Morning", "Good Afternoon" or "Good Evening" based on time.
Shipped as a docker container deployed to kubernetes.

## CI/CD
1. Build new docker image on every commit to master.
2. Run tests on every commit to master and feature branches.
3. Deploy to kubernetes (, if stages succeed)

