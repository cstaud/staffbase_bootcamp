FROM elixir:1.9-alpine as builder

ARG APP_NAME=staffbase_bootcamp
ARG MIX_ENV=prod

ENV APP_NAME=${APP_NAME} \
    MIX_ENV=${MIX_ENV}

WORKDIR /opt/app

RUN \
  apk update && \
  apk upgrade --no-cache && \
  apk add --no-cache \
    build-base && \
  mix local.rebar --force && \
  mix local.hex --force

COPY . .

RUN mix do deps.get, deps.compile, compile

RUN \
  mkdir -p /opt/built && \
  mix release && \
  cp -r _build/${MIX_ENV}/rel/${APP_NAME}/* /opt/built && \

FROM alpine:3.9

ARG APP_NAME=staffbase_bootcamp

RUN apk update && \
    apk add --no-cache \
      bash

ENV REPLACE_OS_VARS=true \
    APP_NAME=${APP_NAME}

WORKDIR /opt/app
COPY --from=builder /opt/built .

CMD trap 'exit' INT; /opt/app/bin/${APP_NAME} start